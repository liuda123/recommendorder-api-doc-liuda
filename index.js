"use strict";
var express = require('express');
var app = express();

app.use(express.static('public'));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    return next();
});


/**
 * @api {get} /recommendorder/api/v1/item Get all items of a dealer, paginated
 * @apiVersion 1.0.0
 * @apiGroup recommendorder_item
 * @apiParam {Number} per_page Number of items per page
 * @apiParam {String} category Retrieved the items of the given category
 * @apiParam {String[]} o The order
 * @apiSampleRequest http://b.ichaomeng.com:3002/recommendorder/api/v1/item
 * @apiSuccess (Response) {Object[]} data All items
 * @apiSuccess (Response) {Object} pagination_meta Metadata for pagination
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *
 * {
 *     "data": [
 *         {
 *             "category": "16--听啤类",
 *             "picture": null,
 *             "sort_level_one": null,
 *             "is_tied_sale": false,
 *             "item_name": "雪花听 清爽300ml*24听",
 *             "price": "44.0000",
 *             "dealer": 1,
 *             "source": "dongsheng",
 *             "packing_specification": "1.0000",
 *             "sort_level_two": null,
 *             "item_id": "12548",
 *             "id": 3836,
 *             "unit": "箱"
 *         },
 *         {
 *             "category": "feipin",
 *             "picture": "13614.jpg",
 *             "sort_level_one": null,
 *             "is_tied_sale": false,
 *             "item_name": "水",
 *             "price": "19.0000",
 *             "dealer": 1,
 *             "source": "dongsheng",
 *             "packing_specification": "50.0000",
 *             "sort_level_two": null,
 *             "item_id": "13614",
 *             "id": 4463,
 *             "unit": "个"
 *         }
 *     ],
 *     "pagination_meta": {
 *         "previous_url": null,
 *         "page_links": [
 *             [
 *                 "http://b.ichaomeng.com:12355/recommendorder/api/v1/item?per_page=2",
 *                 1,
 *                 true,
 *                 false
 *             ],
 *             [
 *                 "http://b.ichaomeng.com:12355/recommendorder/api/v1/item?page=2&per_page=2",
 *                 2,
 *                 false,
 *                 false
 *             ],
 *             [
 *                 "http://b.ichaomeng.com:12355/recommendorder/api/v1/item?page=3&per_page=2",
 *                 3,
 *                 false,
 *                 false
 *             ],
 *             [
 *                 null,
 *                 null,
 *                 false,
 *                 true
 *             ],
 *             [
 *                 "http://b.ichaomeng.com:12355/recommendorder/api/v1/item?page=9816&per_page=2",
 *                 9816,
 *                 false,
 *                 false
 *             ]
 *         ],
 *         "next_url": "http://b.ichaomeng.com:12355/recommendorder/api/v1/item?page=2&per_page=2"
 *     }
 * }
 */


/**
 * @api {get} /recommendorder/api/v1/items_extended Get items of a dealer, with stock num and sorting score
 * @apiVersion 1.0.0
 * @apiGroup recommendorder_item
 */


/**
 * @api {get} /discoverstore/api/v1/searchhistory 获取近期搜索的历史记录
 * @apiVersion 1.0.0
 * @apiGroup DiscoverStore
 * @apiSampleRequest http://b.ichaomeng.com:3002/discoverstore/api/v1/searchhistory
 * @apiSuccess (Response) {Object[]} data 近期搜索历史
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *  {"data":
 *    [{
 *      "place": "华清嘉园",
 *      "lat": 39.9971810000,
 *      "lng": 116.3434880000
 *     },
 *     {
 *      "place": "崔各庄",
 *     "lat": 40.0284370000,
 *      "lng": 116.4991450000
 *     },
 *     {
 *      "place": "天安门",
 *      "lat": 39.9151190000,
 *      "lng": 116.4039630000
 *     }]
 *  }
 * @apiError Error 获取数据失败
 */
app.get('/discoverstore/api/v1/searchhistory', function(req, res) {
    res.json(
        {"data":
            [{
               "place": "华清嘉园",
               "lat": 39.9971810000,
               "lng": 116.3434880000
            },
           {
               "place": "崔各庄",
               "lat": 40.0284370000,
               "lng": 116.4991450000
           },
           {
               "place": "天安门",
               "lat": 39.9151190000,
               "lng": 116.4039630000
           }]
       }
    )
});


/**
 * @api {post} /discoverstore/api/v1/sign 签到
 * @apiVersion 1.0.0
 * @apiGroup DiscoverStore
 * @apiParam {String} place 地点
 * @apiParam {Number} lat 纬度
 * @apiParam {Number} lng 经度
 * @apiSampleRequest http://b.ichaomeng.com:3002/discoverstore/api/v1/sign
 * @apiSuccess (Response) {String} status 操作结果
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *  {
 *      status:"ok"
 *  }
 * @apiError Error 签到失败
 */
app.post('/discoverstore/api/v1/sign', function(req, res) {
    res.json(
        {
            status:"ok"
        }
    )
});


/**
 * @api {post} /discoverstore/api/v1/store 新增商铺
 * @apiVersion 1.0.0
 * @apiGroup DiscoverStore
 * @apiParam {String} place 地址
 * @apiParam {String} name 商家名称
 * @apiParam {String} phone 联系方式
 * @apiParam {String} link_man 联系人
 * @apiParam {String} remarks 备注
 * @apiSampleRequest http://b.ichaomeng.com:3002/discoverstore/api/v1/store
 * @apiSuccess (Response) {String} status 操作结果
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *  {
 *      status:"ok"
 *  }
 * @apiError Error 签到失败
 */
app.post('/discoverstore/api/v1/store', function(req, res) {
    res.json(
        {
            status:"ok"
        }
    )
});


/**
 * @api {delete} /discoverstore/api/v1/store 删除商铺
 * @apiVersion 1.0.0
 * @apiGroup DiscoverStore
 * @apiParam {String} place 地址
 * @apiParam {String} name 商家名称
 * @apiParam {String} remarks 备注
 * @apiSampleRequest http://b.ichaomeng.com:3002/discoverstore/api/v1/store
 * @apiSuccess (Response) {String} status 操作结果
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *  {
 *      status:"ok"
 *  }
 * @apiError Error 签到失败
 */
app.delete('/discoverstore/api/v1/store', function(req, res) {
    res.json(
        {
            status:"ok"
        }
    )
});


/**
 * @api {get} /discoverstore/api/v1/storemsg 获取地图展示的门店信息
 * @apiVersion 1.0.0
 * @apiGroup DiscoverStore
 * @apiParam {Number} lat 业务员位置纬度
 * @apiParam {Number} lng 业务员位置经度
 * @apiSampleRequest http://b.ichaomeng.com:3002/discoverstore/api/v1/storemsg
 * @apiSuccess (Response) {json} data 门店信息
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *  { "data":
 *       {
 *          "supplied":[
 *                         {
 *                          "place": "华清嘉园",
 *                          "name": "世纪华联超市",
 *                          "phone": "15142820718",
 *                          "supplied_time": "2016-10-21 12:00:05",
 *                          "lat": 39.9971810000,
 *                          "lng": 116.3434880000
 *                         },
 *                          {
 *                          "place": "崔各庄",
 *                          "name": "超市发",
 *                          "phone": "15142820718",
 *                          "supplied_time": "2016-10-21 12:00:05",
 *                          "lat": 39.9971810000,
 *                          "lng": 116.3434880000
 *                         }
 *                     ],
 *          "unsupplied":[
 *                         {
 *                          "place": "华清嘉园",
 *                          "name": "超市发",
 *                          "phone": "15142820718",
 *                          "supplied_time": "2016-10-21 12:00:05",
 *                          "lat": 39.9971810000,
 *                          "lng": 116.3434880000
 *                         },
 *                          {
 *                          "place": "崔各庄",
 *                          "name": "世纪华联超市",
 *                          "phone": "15142820718",
 *                          "supplied_time": "2016-10-21 12:00:05",
 *                          "lat": 40.0284370000,
 *                          "lng": 116.4991450000
 *                         }
 *                      ]
 *         }
 *  }
 * @apiError Error 获取数据失败
 */
app.get('/discoverstore/api/v1/storemsg', function(req, res) {
    res.json(
        { "data":
            {
               "supplied":[
                              {
                               "place": "华清嘉园",
                               "name": "世纪华联超市",
                               "phone": "15142820718",
                               "supplied_time": "2016-10-21 12:00:05",
                               "lat": 39.9971810000,
                               "lng": 116.3434880000
                              },
                               {
                               "place": "崔各庄",
                               "name": "超市发",
                               "phone": "15142820718",
                               "supplied_time": "2016-10-21 12:00:05",
                               "lat": 39.9971810000,
                               "lng": 116.3434880000
                              }
                          ],
               "unsupplied":[
                              {
                               "place": "华清嘉园",
                               "name": "超市发",
                               "phone": "15142820718",
                               "store_type": "大型超市",
                               "lat": 39.9971810000,
                               "lng": 116.3434880000
                              },
                               {
                                "place": "崔各庄",
                                "name": "世纪华联超市",
                                "store_type": "小型超市",
                                "phone": "15142820718",
                                "supplied_time": "2016-10-21",
                                "lat": 40.0284370000,
                                "lng": 116.4991450000
                              }
                          ]
            }
       }
    )
});

app.listen(3002, '0.0.0.0', function() {
    console.log(' * CM API up and running...');
});
